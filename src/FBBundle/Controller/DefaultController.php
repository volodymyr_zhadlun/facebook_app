<?php

namespace FBBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * DefaultController constructor.
     *
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @return Response
     */
    public function indexAction() : Response
    {
        return new Response(
            $this->twig->render('FBBundle:Default:index.html.twig')
        );
    }
}
