<?php


namespace FBBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * TestController constructor.
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $manager = $this->get('manager.tests');

        $testCase = $manager->find(1);

        if ($request->get('username')) {
            $testService = $this->get('service.test');
            $userTest = $testService->createTest($testCase, $request->get('username'));
            $selectedQuestions = $request->get('question');

            foreach ($testCase->getQuestions() as $question) {
                if (!in_array($question->getId(), $selectedQuestions)) continue;

                foreach ($question->getAnswers() as $answer) {
                    $selectedAnswer = $request->get('answer_' . $question->getId());
                    if ($answer->getId() != $selectedAnswer) continue;

                    $testService->addAnswer($userTest, $question, $answer);
                }
            }

            return new Response(
                $this->twig->render('FBBundle:Tests:create_complete.html.twig', [
                    'userTest' => $userTest
                ])
            );
        }

        return new Response(
            $this->twig->render('FBBundle:Tests:create.html.twig', [
                'questions' => $testCase->getQuestions()
            ])
        );
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function showAction(Request $request)
    {
        $manager = $this->get('manager.usertests');
        $userTest = $manager->find($request->get('id'));
        if ($request->getMethod() == 'POST') {
            $testService = $this->get('service.test');
            return new Response(
                $this->twig->render('FBBundle:Tests:result.html.twig', [
                    'userTest' => $userTest,
                    'score' => $testService->checkScore($userTest, $request)
                ])
            );
        }
        return new Response(
            $this->twig->render('FBBundle:Tests:show.html.twig', [
                'userTest' => $userTest
            ])
        );
    }
}
