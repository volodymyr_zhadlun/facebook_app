<?php


namespace FBBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use FBBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $fbUserId = $request->get('userID');
        $manager = $this->get('manager.user');

        try {
            $user = $manager->find($fbUserId);
        } catch (EntityNotFoundException $e) {
            //create new user
            $user = new User();
            $user->setFbUserId($fbUserId);
            $manager->save($user);
            //update user from db
            $user = $manager->find($fbUserId);
        }

        if ($user->getValidUntil() < time()) {
            $accessToken = $request->get('accessToken');
            $tokenResponse = $this->get('service.token')->generateToken($accessToken);
            $user->setToken($tokenResponse->access_token);
            $user->setValidUntil(time() + $tokenResponse->expires_in - 3600);
            $manager->save($user);
        }

        if ($user->getUpdatedAt() < (time() - 3600)) {
            //update user info
            $userService = $this->get('service.user');
            $userInfo = $userService->getUserInfo($user);
            $user->setName($userInfo->name);
            $userPicture = $userService->getUserPicture($user);
            $user->setImage($userPicture->data->url);
            $user->setUpdatedAt(time());
            $manager->save($user);
        }

        $response = $this->get('jms_serializer')->serialize($user, 'json');

        return new JsonResponse($response, 200, [], true);
    }
}