<?php

namespace FBBundle\Service;

use FBBundle\Entity\User as UserEntity;

class User
{
    /**
     * @var GraphApiClient
     */
    private $apiClient;

    /**
     * @param GraphApiClient $apiClient
     */
    public function setApiClient(GraphApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param UserEntity $user
     * @return mixed
     */
    public function getUserInfo(UserEntity $user)
    {
        $this->apiClient->setAuth($user->getToken());

        return $this->apiClient->request('v2.10/' . $user->getFbUserId(), []);
    }

    /**
     * @param UserEntity $user
     * @return mixed
     */
    public function getUserPicture(UserEntity $user)
    {
        $this->apiClient->setAuth($user->getToken());

        $query = ['redirect' => 'false'];

        return $this->apiClient->request('v2.10/' . $user->getFbUserId() . '/picture', $query);
    }
}
