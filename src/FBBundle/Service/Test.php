<?php


namespace FBBundle\Service;


use FBBundle\Entity\TestAnswer;
use FBBundle\Entity\TestCase;
use FBBundle\Entity\TestQuestion;
use FBBundle\Entity\UserAnswer;
use FBBundle\Entity\UserTest;
use FBBundle\Manager\UserAnswers;
use FBBundle\Manager\UserTests;
use Symfony\Component\HttpFoundation\Request;

class Test
{
    /**
     * @var UserTests
     */
    private $userTestsManager;

    /**
     * @var UserAnswers
     */
    private $userAnswersManager;

    /**
     * @param UserTests $userTests
     */
    public function setUserTestsManager(UserTests $userTests)
    {
        $this->userTestsManager = $userTests;
    }

    public function setUserAnswersManager(UserAnswers $userAnswers)
    {
        $this->userAnswersManager = $userAnswers;
    }

    /**
     * @param TestCase $test
     * @param string $name
     * @return UserTest
     */
    public function createTest(TestCase $test, string $name)
    {
        $entity = new UserTest();
        $entity->setName($name);
        $entity->setTestCase($test);
        $this->userTestsManager->save($entity);
        return $entity;
    }

    /**
     * @param UserTest $userTest
     * @param TestQuestion $question
     * @param TestAnswer $answer
     * @return UserAnswer
     */
    public function addAnswer(UserTest $userTest, TestQuestion $question, TestAnswer $answer)
    {
        $userAnswerEntity = new UserAnswer();
        $userAnswerEntity->setUserTest($userTest)
            ->setQuestion($question)
            ->setAnswer($answer);
        $this->userAnswersManager->save($userAnswerEntity);
        return $userAnswerEntity;
    }

    /**
     * @param UserTest $userTest
     * @param Request $request
     * @return int
     */
    public function checkScore(UserTest $userTest, Request $request)
    {
        $total = 0;
        $correct = 0;
        foreach ($userTest->getUserAnswers() as $userAnswer) {
            $answer = $request->get('answer_' . $userAnswer->getQuestion()->getId());
            if ($answer == $userAnswer->getAnswer()->getId()) {
                $correct++;
            }
            $total++;
        }
        return ceil(($correct * 100) / $total);
    }
}