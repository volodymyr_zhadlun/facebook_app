<?php

namespace FBBundle\Service;

class GraphApiClient
{
    const API_URI = 'https://graph.facebook.com/';
    const CLIENT_ID = '361763250941822';
    const CLIENT_SECRET = 'aa08e44d4c8adb0c8cbfed44fe2fec1b';

    /**
     * @var resource
     */
    private $curl;

    public function __construct()
    {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
    }

    /**
     * @param string $accessToken
     */
    public function setAuth(string $accessToken)
    {
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $accessToken]);
    }

    /**
     * @param string $method
     * @param array $request
     * @return mixed
     */
    public function request(string $method, array $request)
    {
        curl_setopt($this->curl, CURLOPT_URL,
            static::API_URI . $method . '?' . http_build_query($this->prepareRequest($request)));
        return json_decode(curl_exec($this->curl));
    }

    /**
     * @param array $request
     * @return array
     */
    protected function prepareRequest(array $request): array
    {
        $request['client_id'] = static::CLIENT_ID;
        $request['client_secret'] = static::CLIENT_SECRET;
        return $request;
    }
}
