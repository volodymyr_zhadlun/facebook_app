<?php

namespace FBBundle\Service;

class Token
{
    /**
     * @var GraphApiClient
     */
    private $apiClient;

    /**
     * @param GraphApiClient $apiClient
     */
    public function setApiClient(GraphApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param string $accessToken
     * @return mixed
     */
    public function generateToken(string $accessToken)
    {
        $request['grant_type'] = 'fb_exchange_token';
        $request['fb_exchange_token'] = $accessToken;

        return $this->apiClient->request('oauth/access_token', $request);
    }
}
