<?php


namespace FBBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use FBBundle\Entity\UserTest;

class UserTests
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(UserTest::class);
    }

    /**
     * @param null $id
     * @return UserTest
     * @throws EntityNotFoundException
     */
    public function find($id = null)
    {
        $entity = $this->repository->findOneBy(['id' => $id]);

        if (null === $entity) {
            throw new EntityNotFoundException();
        }

        return $entity;
    }

    /**
     * @param $entity
     */
    public function save($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }
}