<?php


namespace FBBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\EntityRepository;
use FBBundle\Entity\TestCase;

class Tests
{
    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(TestCase::class);
    }

    /**
     * @param null $id
     * @return TestCase
     * @throws EntityNotFoundException
     */
    public function find($id = null)
    {
        $entity = $this->repository->findOneBy(['id' => $id]);

        if (null === $entity) {
            throw new EntityNotFoundException();
        }

        return $entity;
    }
}