<?php


namespace FBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="test_cases")
 * @ORM\Entity
 */
class TestCase
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=30)
     */
    private $name;

    /**
     * @var TestQuestion[]
     * @ORM\OneToMany(targetEntity="TestQuestion", mappedBy="testCase")
     */
    private $questions;

    /**
     * @return TestQuestion[]
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param TestQuestion $questions
     * @return TestCase
     */
    public function setQuestions(TestQuestion $questions): TestCase
    {
        $this->questions = $questions;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TestCase
     */
    public function setId(int $id): TestCase
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TestCase
     */
    public function setName(string $name): TestCase
    {
        $this->name = $name;
        return $this;
    }
}
