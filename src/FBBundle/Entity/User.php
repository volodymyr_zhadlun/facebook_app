<?php

namespace FBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Class User
 * @ORM\Table(name="users")
 * @ORM\Entity
 */
class User
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=30)
     * @JMS\Type("string")
     */
    private $fbUserId;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=true)
     * @JMS\Type("string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=512, nullable=true)
     * @JMS\Type("string")
     */
    private $image;

    /**
     * @var string
     * @ORM\Column(type="string", length=512, nullable=true)
     * @JMS\Exclude()
     */
    private $token;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Exclude()
     */
    private $validUntil;

    /**
     * @var string
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Exclude()
     */
    private $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getFbUserId()
    {
        return $this->fbUserId;
    }

    /**
     * @param string $fbUserId
     * @return User
     */
    public function setFbUserId(string $fbUserId): User
    {
        $this->fbUserId = $fbUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return User
     */
    public function setImage(string $image): User
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return User
     */
    public function setToken(string $token): User
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return integer
     */
    public function getValidUntil()
    {
        return $this->validUntil;
    }

    /**
     * @param string $validUntil
     * @return User
     */
    public function setValidUntil(string $validUntil): User
    {
        $this->validUntil = $validUntil;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     * @return User
     */
    public function setUpdatedAt(string $updatedAt): User
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
