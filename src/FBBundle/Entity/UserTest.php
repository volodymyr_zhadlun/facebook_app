<?php


namespace FBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_tests")
 * @ORM\Entity
 */
class UserTest
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var TestCase
     * @ORM\ManyToOne(targetEntity="TestCase", inversedBy="id")
     * @ORM\JoinColumn(name="test_id", referencedColumnName="id")
     */
    private $testCase;

    /**
     * @var string
     * @ORM\Column(type="string", length=30)
     */
    private $name;

    /**
     * @var UserAnswer[]
     * @ORM\OneToMany(targetEntity="UserAnswer", mappedBy="userTest")
     */
    private $userAnswers;

    /**
     * @return UserAnswer[]
     */
    public function getUserAnswers()
    {
        return $this->userAnswers;
    }

    /**
     * @param UserAnswer[] $userAnswers
     * @return UserTest
     */
    public function setUserAnswers($userAnswers): UserTest
    {
        $this->userAnswers = $userAnswers;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserTest
     */
    public function setId(int $id): UserTest
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return TestCase
     */
    public function getTestCase(): TestCase
    {
        return $this->testCase;
    }

    /**
     * @param TestCase $testCase
     * @return UserTest
     */
    public function setTestCase(TestCase $testCase): UserTest
    {
        $this->testCase = $testCase;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UserTest
     */
    public function setName(string $name): UserTest
    {
        $this->name = $name;
        return $this;
    }
}