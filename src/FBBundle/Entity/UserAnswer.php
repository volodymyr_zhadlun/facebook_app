<?php


namespace FBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_answers")
 * @ORM\Entity
 */
class UserAnswer
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var UserTest
     * @ORM\ManyToOne(targetEntity="UserTest", inversedBy="id")
     * @ORM\JoinColumn(name="user_test_id", referencedColumnName="id")
     */
    private $userTest;

    /**
     * @var TestQuestion
     * @ORM\ManyToOne(targetEntity="TestQuestion", inversedBy="id")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @var TestAnswer
     * @ORM\ManyToOne(targetEntity="TestAnswer", inversedBy="id")
     * @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     */
    private $answer;

    /**
     * @return UserTest
     */
    public function getUserTest(): UserTest
    {
        return $this->userTest;
    }

    /**
     * @param UserTest $userTest
     * @return UserAnswer
     */
    public function setUserTest(UserTest $userTest): UserAnswer
    {
        $this->userTest = $userTest;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserAnswer
     */
    public function setId(int $id): UserAnswer
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return TestQuestion
     */
    public function getQuestion(): TestQuestion
    {
        return $this->question;
    }

    /**
     * @param TestQuestion $question
     * @return UserAnswer
     */
    public function setQuestion(TestQuestion $question): UserAnswer
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return TestAnswer
     */
    public function getAnswer(): TestAnswer
    {
        return $this->answer;
    }

    /**
     * @param TestAnswer $answer
     * @return UserAnswer
     */
    public function setAnswer(TestAnswer $answer): UserAnswer
    {
        $this->answer = $answer;
        return $this;
    }
}