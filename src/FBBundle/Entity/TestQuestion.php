<?php


namespace FBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="test_questions")
 * @ORM\Entity
 */
class TestQuestion
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var TestCase
     * @ORM\ManyToOne(targetEntity="TestCase", inversedBy="id")
     * @ORM\JoinColumn(name="test_id", referencedColumnName="id")
     */
    private $testCase;

    /**
     * @var string
     * @ORM\Column(type="string", length=30)
     */
    private $question;

    /**
     * @var TestAnswer[]
     * @ORM\OneToMany(targetEntity="TestAnswer", mappedBy="question")
     */
    private $answers;

    /**
     * @return TestAnswer[]
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param TestQuestion $answers
     * @return TestQuestion
     */
    public function setAnswers(TestQuestion $answers): TestQuestion
    {
        $this->answers = $answers;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TestQuestion
     */
    public function setId(int $id): TestQuestion
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return TestCase
     */
    public function getTestCase(): TestCase
    {
        return $this->testCase;
    }

    /**
     * @param TestCase $testCase
     * @return TestQuestion
     */
    public function setTestCase(TestCase $testCase): TestQuestion
    {
        $this->testCase = $testCase;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * @param string $question
     * @return TestQuestion
     */
    public function setQuestion(string $question): TestQuestion
    {
        $this->question = $question;
        return $this;
    }
}
