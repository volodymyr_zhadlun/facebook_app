<?php


namespace FBBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="test_answers")
 * @ORM\Entity
 */
class TestAnswer
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="TestQuestion", inversedBy="id")
     * @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     */
    private $question;

    /**
     * @var string
     * @ORM\Column(type="string", length=30)
     */
    private $answer;

    /**
     * @return int
     */
    public function getQuestion(): int
    {
        return $this->question;
    }

    /**
     * @param int $question
     * @return TestAnswer
     */
    public function setQuestion(int $question): TestAnswer
    {
        $this->question = $question;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TestAnswer
     */
    public function setId(int $id): TestAnswer
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer;
    }

    /**
     * @param string $answer
     * @return TestAnswer
     */
    public function setAnswer(string $answer): TestAnswer
    {
        $this->answer = $answer;
        return $this;
    }
}
