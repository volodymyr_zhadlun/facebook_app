<?php

namespace FBBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use FBBundle\Entity\UserTest;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use FBBundle\Entity\TestCase as TestCaseEntity;

class TestsTest extends TestCase
{
    /**
     * @var EntityManager | PHPUnit_Framework_MockObject_MockObject
     */
    private $entityManager;

    /**
     * @var EntityRepository | PHPUnit_Framework_MockObject_MockObject
     */
    private $repository;

    /**
     * @var Tests | PHPUnit_Framework_MockObject_MockObject
     */
    private $manager;

    /**
     * @var UserTest | PHPUnit_Framework_MockObject_MockObject
     */
    private $entity;

    public function setUp()
    {
        $this->repository = $this->createMock(EntityRepository::class);
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->entityManager->method('getRepository')->willReturn($this->repository);
        $this->entity = $this->createMock(TestCaseEntity::class);
        $this->manager = new Tests($this->entityManager);
    }

    public function test_find()
    {
        $this->repository->method('findOneBy')->with(['id'=>1])->willReturn($this->entity);
        $this->assertEquals($this->entity, $this->manager->find(1));
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function test_findException()
    {
        $this->manager->find(1);
    }
}