<?php

namespace FBBundle\Service;

use FBBundle\Entity\TestAnswer;
use FBBundle\Entity\TestQuestion;
use FBBundle\Entity\UserAnswer;
use FBBundle\Entity\UserTest;
use FBBundle\Manager\UserAnswers;
use FBBundle\Manager\UserTests;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use FBBundle\Entity\TestCase as TestCaseEntity;
use Symfony\Component\HttpFoundation\Request;

class TestTest extends TestCase
{
    /**
     * @var UserTests | PHPUnit_Framework_MockObject_MockObject
     */
    private $userTestsManager;
    /**
     * @var UserAnswers | PHPUnit_Framework_MockObject_MockObject
     */
    private $userAnswersManager;
    /**
     * @var Test
     */
    private $service;
    /**
     * @var TestCaseEntity | PHPUnit_Framework_MockObject_MockObject
     */
    private $testCaseEntity;
    /**
     * @var TestQuestion | PHPUnit_Framework_MockObject_MockObject
     */
    private $testQuestionEntity;
    /**
     * @var TestAnswer | PHPUnit_Framework_MockObject_MockObject
     */
    private $testAnswerEntity;
    /**
     * @var UserTest | PHPUnit_Framework_MockObject_MockObject
     */
    private $userTestEntity;
    /**
     * @var UserAnswer | PHPUnit_Framework_MockObject_MockObject
     */
    private $userAnswerEntity;

    public function setUp()
    {
        $this->userTestsManager = $this->createMock(UserTests::class);
        $this->userAnswersManager = $this->createMock(UserAnswers::class);
        $this->testCaseEntity = $this->createMock(TestCaseEntity::class);
        $this->testQuestionEntity = $this->createMock(TestQuestion::class);
        $this->testAnswerEntity = $this->createMock(TestAnswer::class);
        $this->userTestEntity = $this->createMock(UserTest::class);
        $this->userAnswerEntity = $this->createMock(UserAnswer::class);
        $this->service = new Test();
        $this->service->setUserTestsManager($this->userTestsManager);
        $this->service->setUserAnswersManager($this->userAnswersManager);
    }

    public function test_create()
    {
        $this->userTestsManager->expects(static::once())->method('save');
        $entity = $this->service->createTest($this->testCaseEntity, 'test');
        $this->assertEquals('test', $entity->getName());
        $this->assertEquals($this->testCaseEntity, $entity->getTestCase());
    }

    public function test_addAnswer()
    {
        $this->userAnswersManager->expects(static::once())->method('save');
        $entity = $this->service->addAnswer($this->userTestEntity, $this->testQuestionEntity, $this->testAnswerEntity);
        $this->assertEquals($this->userTestEntity, $entity->getUserTest());
        $this->assertEquals($this->testQuestionEntity, $entity->getQuestion());
        $this->assertEquals($this->testAnswerEntity, $entity->getAnswer());
    }

    public function test_checkScore()
    {
        /** @var Request | PHPUnit_Framework_MockObject_MockObject $request */
        $request = $this->createMock(Request::class);
        $request->expects(static::once())->method('get')->with('answer_1')->willReturn(1);
        $this->testQuestionEntity->expects(static::once())->method('getId')->willReturn(1);
        $this->testAnswerEntity->expects(static::once())->method('getId')->willReturn(1);
        $this->userAnswerEntity->expects(static::once())->method('getQuestion')->willReturn($this->testQuestionEntity);
        $this->userAnswerEntity->expects(static::once())->method('getAnswer')->willReturn($this->testAnswerEntity);
        $this->userTestEntity->method('getUserAnswers')->willReturn([$this->userAnswerEntity]);
        $this->assertEquals(100,$this->service->checkScore($this->userTestEntity, $request));
    }
}
